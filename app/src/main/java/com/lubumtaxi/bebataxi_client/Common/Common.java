package com.lubumtaxi.bebataxi_client.Common;

import com.lubumtaxi.bebataxi_client.Remote.FCMClient;
import com.lubumtaxi.bebataxi_client.Remote.IFCMService;

public class Common {
    public static final String driver_tbl = "Drivers"; // contient les information de localisation du chauffeur
    public static final String user_driver_tbl = "DriversInformation";//contient les information d'identification du chauffeur
    public static final String user_rider_tbl = "RidersInformation";//contient les information d'identification du client
    public static final String pickup_request_tbl = "PickupRequest";// contient les information de localisation du client
    public static String tokens_tbl="tokens";

    public static final String baseURL = "http://maps.googleapis.com";
    public static final String fcmURL = "http://fcm.googleapis.com";


    public static IFCMService getFCMService(){
        return FCMClient.getClient(fcmURL).create(IFCMService.class);
    }
}
