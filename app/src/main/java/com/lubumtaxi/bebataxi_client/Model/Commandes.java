package com.lubumtaxi.bebataxi_client.Model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Commandes {
    public String customer_id;
    public String driver_email;

    public Commandes() {
    }

    public Commandes(String customer_id, String driver_email) {
        this.customer_id = customer_id;
        this.driver_email = driver_email;
    }
    @Exclude
    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("customer_id", customer_id);
        result.put("driver_email", driver_email);

        return result;
    }
}
