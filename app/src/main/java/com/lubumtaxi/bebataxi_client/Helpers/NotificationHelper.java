package com.lubumtaxi.bebataxi_client.Helpers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.lubumtaxi.bebataxi_client.R;

public class NotificationHelper  extends ContextWrapper{
    private static final String BEBATAXI_CHANNEL_ID = "com.lubumtaxi.bebataxi_client";
    private static final String BEBATAXI_CHANNEL_NAME = "bebataxi client";
    private NotificationManager manager;

    public NotificationHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            createChannels();
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChannels() {
        NotificationChannel bebataxichannel = new NotificationChannel(BEBATAXI_CHANNEL_ID,
                BEBATAXI_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT);
        bebataxichannel.enableLights(true);
        bebataxichannel.enableVibration(true);
        bebataxichannel.setLightColor(Color.GRAY);
        bebataxichannel.setLockscreenVisibility(android.app.Notification.VISIBILITY_PRIVATE);

        getManager().createNotificationChannel(bebataxichannel);
    }

    public NotificationManager getManager() {
        if (manager == null)
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        return manager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getBebaNotification(String title, String content,
                                                    PendingIntent contentIntent, Uri soundUri){
        return new Notification.Builder(getApplicationContext(), BEBATAXI_CHANNEL_ID)
                .setContentText(content)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_car_notification);
    }
}
