# Taxi Web Client

This is an Uber clone project for the client helping them to: 
    
  - Create and account and loggin
  - Search For a Taxi
  - Select a taxi and call it
  - If the taxi driver accept this will notify the user once the taxi is there
  - Calculate Distance
  - etc
  

# Future Features!

  - Adding Rating of taxi drivers
  - Improve UI and UX

# Getting Started

Just clone the project using
```sh
git clone https://gitlab.com/alexandre2908/TaxiAsk_Client.git
```
and then open it in Android studio and then customize it as you want

Here is the driver App: 

```sh
https://gitlab.com/alexandre2908/TaxiAsk.git
```


# Prerequisite
- Advanced Knowledge of java and mobile developpement
- Advanced Knowledge of google map 

### Author


* [Axel Mwenze](https:axelmwenze.com) - Software Engineer and Fullstack Web Developper


License
----

MIT


**Free Software, Hell Yeah!**
